<?php require_once('../config.php'); include_once('../shared/header.php'); ?>
		<header class="container-fluid parallax" id="header-image">
			<div class="row image small" style="background-image:url('../shared/img/lyceum-medium.jpg'); filter:progid:DXImageTransform.Microsoft.AlphaImageLoader(src='../shared/img/lyceum-medium.jpg', sizingMethod='scale'); -ms-filter:"progid:DXImageTransform.Microsoft.AlphaImageLoader(src='../shared/img/lyceum-medium.jpg',sizingMethod='scale')";">
				<div class="caption col-xs-12">
					<h4>Lyceum aan Zee</h4>
					<h1>Roosters</h1>
				</div>
			</div>
		</header>
		
		<div class="container lyceum" id="main-panel">
			<div class="row">
				<ul class="nav nav-blocks nav-justified" id="primary-links">
					<li class="active" role="presentation"><a href="rooster.php"><h3>Roosters</h3></a></li>
					<li role="presentation"><a href="http://elo.lyceumaanzee.nl"><h3>ELO</h3></a></li>
					<li role="presentation"><a href="http://scholenaanzee.swp.nl"><h3>Magister</h3></a></li>
					<li role="presentation"><a href="#"><h3>Fotoboeken</h3></a></li>
				</ul>
				<ul class="nav nav-blocks nav-justified" id="secondary-links">
					<li role="presentation"><a href="#">Agenda</a></li>
					<li role="presentation" class="dropdown">
						<a class="dropdown-toggle" data-toggle="dropdown" href="#" role="button" aria-expanded="false">Reglementen</a>
						<ul class="dropdown-menu" role="menu">
							<li role="presentation"><a href="#">Bevorderingsnormen</a></li>
							<li role="presentation"><a href="#">PTA's</a></li>
							<li role="presentation"><a href="#">Rekenen</a></li>
							<li role="presentation"><a href="#">Eindexamen</a></li>
						</ul>
					</li>
					<li role="presentation"><a href="#">LLR/MR/OR</a></li>
					<li role="presentation"><a href="#">Schoolkrant</a></li>
					<li role="presentation" class="dropdown">
						<a class="dropdown-toggle" data-toggle="dropdown" href="#" role="button" aria-expanded="false"><span class="hidden-sm hidden-md">Uitgaande</span> Brieven</a>
						<ul class="dropdown-menu" role="menu">
							<li role="presentation"><a href="#">Leerjaar 1</a></li>
							<li role="presentation"><a href="#">Leerjaar 2</a></li>
							<li role="presentation"><a href="#">Leerjaar 3</a></li>
							<li role="presentation"><a href="#">Leerjaar 4</a></li>
							<li role="presentation"><a href="#">Leerjaar 5</a></li>
							<li role="presentation"><a href="#">Leerjaar 6</a></li>
							<li role="presentation"><a href="#">Nieuwsbrieven</a></li>
						</ul>
					</li>
					<li role="presentation"><a href="#">LOB</a></li>
					<li role="presentation" class="dropdown">
						<a class="dropdown-toggle" data-toggle="dropdown" href="#" role="button" aria-expanded="false">
							Ouders
						</a>
						<ul class="dropdown-menu" role="menu">
							<li role="presentation"><a href="#">Magister <i class="fa fa-external-link-square"></i></a></li>
							<li role="presentation"><a href="#">Absentie</a></li>
							<li role="presentation"><a href="#">Schoolgids <i class="fa fa-cloud-download"></i></a></li>
						</ul>
					</li>
					<li role="presentation" class="dropdown">
						<a class="dropdown-toggle" data-toggle="dropdown" href="#" role="button" aria-expanded="false">
							Personeel
						</a>
						<ul class="dropdown-menu" role="menu">
							<li role="presentation"><a href="#">YouForce <i class="fa fa-external-link-square"></i></a></li>
							<li role="presentation"><a href="#">Quickcard Werving <i class="fa fa-cloud-download"></i></a></li>
							<li role="presentation"><a href="#">Quickcard Verzuim <i class="fa fa-cloud-download"></i></a></li>
						</ul>
					</li>
					<li role="presentation"><a href="#">Contact</a></li>
				</ul>
			</div>
			
			<div class="row">
				<div class="col-xs-12 col-sm-12 col-md-9 col-lg-8" id="schedules">
					<header class=" clearfix">
						<h2 class="pull-left">Mijn Rooster<br>
						<small><span class="hidden-xs">Het rooster voor </span><span class="schedule-name">Thomas Izaksson</span> (<span class="schedule-number">122109</span>)</small></h2> 
					</header>
					
					<div class="schedule-wrapper">
						
					</div>
					
				</div>
				<div class="col-xs-12 col-sm-12 col-md-3 col-lg-4">
					<h3></h3>
					<input id="leerlingnummer-input" placeholder="Leerlingnummer">
					<button id="leerlingnummer-button" class="btn btn-default">Magische button</button>
				</div>
			</div>
		</div>

<?php include_once('../shared/footer.php'); ?>