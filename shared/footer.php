		<footer class="container-fluid" >
			<div class="container">
				<div class="row">
					<div class="col-xs-12 col-sm-4 col-md-4 col-lg-4">
						<h3>Over Scholen aan Zee</h3>
						<p>Lorem ipsum dolor sit amet, consectetur adipiscing elit. Proin egestas volutpat ante sed pretium. Proin tempus aliquet dui, vitae commodo erat varius ac. Quisque aliquam vestibulum tincidunt. Phasellus molestie ligula et purus facilisis volutpat eget nec enim.</p>
					</div>
					
					<div class="col-xs-12 col-sm-4 col-md-4 col-lg-4">
						<h3>Snel naar..</h3>
						<ul class="list-unstyled">
							<li><a href="#">Link 1</a></li>
							<li><a href="#">Link 2</a></li>
							<li><a href="#">Link 3</a></li>
						</ul>
					</div>
					
					<div class="col-xs-12 col-sm-4 col-md-4 col-lg-4">
						<h3>Externe links</h3>
						<ul class="list-unstyled">
							<li><a href="#">Link 1</a></li>
							<li><a href="#">Link 2</a></li>
							<li><a href="#">Link 3</a></li>
						</ul>
					</div>
				</div>
				<hr style="border-color:#555">
				<div class="row">
					<div class="col-xs-12">
						<h4 style="text-align: center;">De ontwerpen van alle pagina's op deze website zijn &copy; Thomas Izaksson en Zeno Kapitein 2015<br>
							<small style="text-align: center;">Deze website is bedoeld als testcase en is geen officieel deel van Scholen aan Zee.</small></h4>
					</div>
				</div>
			</div>
		</footer>

		<!-- jQuery (necessary for Bootstrap's JavaScript plugins) -->
		<script src="https://ajax.googleapis.com/ajax/libs/jquery/1.11.1/jquery.min.js"></script>
		<!-- Include all compiled plugins (below), or include individual files as needed -->
		<script src="<?= SCRIPT_ROOT ?>shared/js/bootstrap.min.js"></script>
		<script src="<?= SCRIPT_ROOT ?>shared/js/jquery.mobile.custom.min.js"></script>
		<script src="<?= SCRIPT_ROOT ?>shared/js/jquery.xdomainajax.js"></script>
		<script src="<?= SCRIPT_ROOT ?>shared/js/scripts.js"></script>
	</body>
</html>