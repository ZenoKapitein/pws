$(document).ready(function() { 
	
	// Making swipe gestures work on carousels. 
	$(".carousel").swiperight(function() {  
		$(this).carousel('prev');  
	});  
	$(".carousel").swipeleft(function() {  
		$(this).carousel('next');  
	});  
	
	// Initiate tooltips.
	$(function () { $('[data-toggle="tooltip"]').tooltip(); });
	$(function () { $('.event a').popover({
			html: true,
			content: function() {
				return $(this).find('.popoverContent').html();
			}
		});
	});
	
	
	// Function used to remove a value from arrays.
	// Used in the "filter buttons" for announcements.
	Array.prototype.remove = function() {
	    var what, a = arguments, L = a.length, ax;
	    while (L && this.length) {
	        what = a[--L];
	        while ((ax = this.indexOf(what)) !== -1) {
	            this.splice(ax, 1);
	        }
	    }
	    return this;
	};
	
	// Function used to check if one array contains all values from another array.
	// Used in the "filter buttons" for announcements.
	function containsAll(needles, haystack){ 
	  for(var i = 0 , len = needles.length; i < len; i++){
	     if($.inArray(needles[i], haystack) === -1) { return false; }
	  }
	  return true;
	}
	
	
	
	
	var docroot = $('body').data('root');
	// This controls if the navbar is "solid" or not.
	// Fires on document.ready, scroll and window.resize
	function headerColor() {
		if(windowWidth > 768) {
			if(scrollPos >= headerBottom - headerContentHeight - 2*navbarHeight) {
				$('.navbar').addClass('solid');
				$('.navbar-brand img').attr('src',docroot+'shared/img/SAZ-logo-header.png');
			}
			else {
				$('.navbar').removeClass('solid');
				$('.navbar-brand img').attr('src',docroot+'shared/img/SAZ-logo-header-big.png');
			}
		}
		
		else {
			if(scrollPos > navbarHeight) {
				$('.navbar').addClass('solid');
				$('.navbar-brand img').attr('src',docroot+'shared/img/SAZ-logo-header.png');
			}
			else {
				$('.navbar').removeClass('solid');
				$('.navbar-brand img').attr('src',docroot+'shared/img/SAZ-logo-header-big.png');
			}
		}
	}
	
	// Setting variables for headerColor() function.
	// Some of these don't have to be changed after being initiated once or on other events.
	var scrollPos = $(window).scrollTop(),
	offset = $('#header-image').offset().top,
	headerHeight = $('#header-image').height(),
	headerContentHeight = $('.carousel .item.active .carousel-caption, #header-image .caption').height(),
	navbarHeight = 50,
	headerBottom = offset + headerHeight,
	windowWidth = $(window).width();
	
	headerColor(); // Fire on load
	
	$(window).scroll(function() {
		scrollPos = $(window).scrollTop();
		headerColor(); // Fire on scroll
		
		// Check the navbar logo and shrink it if needed.
		if(scrollPos >= navbarHeight) {
			// User has scrolled down, let's shrink!
			$('.navbar').removeClass('big-logo');
		}
		else {
			// User has scrolled back up, let's make it bigger again.
			$('.navbar').addClass('big-logo');
		}
	}); 
		
	$(window).resize(function() {
		// On window resize some values are bound to change. These don't have to be calculated on every scroll event.
		// Let's recalculate them now.
		windowWidth = $(window).width();
		headerHeight = $('#header-image').height();
		headerContentHeight = $('.carousel .item.active .carousel-caption, #header-image .caption').height();
		navbarHeight = $('.navbar').height();
	});
	
	
	
	
	// Set the filterlist variable so that it's always accessible.
	var filterlist = [];
	
	// This calculates the filter buttons.
	$('#announcement-filter .btn-group button').click(function() {
		var filter = $(this).data('filter').toString();
		
		if($(this).hasClass('active')) {
			// Add a filter to the criteria list.
			$(this).removeClass('active');
			filterlist.remove(filter);
			
			if(filter === 'havo') {
				$('#announcement-filter button[data-filter="6"]').attr('disabled',null);
			}
		}
		
		else {
			// Remove a filter from the criteria list.
			$(this).addClass('active');
			filterlist.push(filter);
			
			if(filter === 'havo') {
				$('#announcement-filter button[data-filter="6"]').attr('disabled','disabled').removeClass('active');
				filterlist.remove('6');
			}
		}
		
		var shown = [];
		// On every button click, re-evaluate all the shown/hidden posts.
		$('#announcements .announcement').each(function() {
			var groups = $(this).data('groups').split(',');

			if(containsAll(filterlist, groups)) {
				// This post meets all the criteria, so let's show it.
				$(this).slideDown(350);
				shown.push($(this));
			}
			else {
				// This post doesn't meet all the criteria (anymore), so let's hide it.
				shown.remove($(this));
				$(this).slideUp(350);
			}
		});
		
		if(shown.length === 0 ) { $('#announcements .no-announcements').slideDown(350); }
		else { $('#announcements .no-announcements').slideUp(350); }
		
		
		
		// When there's no filter selected, show everything.
		if(filterlist.length === 0) {
			$('#announcements .announcement').show();
		}
		
	});

	
	$('#leerlingnummer-button').click(function() {
		var llrnmr = $('#leerlingnummer-input').val();
		$(".schedule-wrapper").load(docroot+"shared/processRooster.php?leerlingnummer="+llrnmr+" #content", function() {
			var schedule_name = $('.schedule-wrapper #content h2').text();
			$('.schedule-name').text(schedule_name.replace(/.*?voor.(.*)/,'$1'));
			$('.schedule-number').text(llrnmr);
			
			$('.schedule-wrapper #content').contents(':not(#rooster)').remove();
			$('#rooster').unwrap();
			$('.roostercontainer').unwrap();
			$('#hoverinfo, #hover_caller_id').remove();
			
			$('.roosterdeel').unwrap().unwrap().unwrap().unwrap().unwrap();
			$('.nobr').each(function() { $(this).html($(this).parents('table.container').attr('onclick')); });
			$('.nobr').unwrap().unwrap().unwrap().unwrap();
			
			$("td b").contents().unwrap().remove();
			
			$('tr > .pic').unwrap().remove();
			$('.pic').remove();
			$('.nobr a').unwrap();
			
			$('td').each(function(){
				var re = /show.*?<br><br><br>(.*?<br>)<br>(.*?<br>)<br>(.*?)<br>.*/; 
				var str = $(this).html();
				var subst = '$1 $2 $3'; 
			 
				var result = str.replace(re, subst);
				$(this).html(result);
			});

			for(n = 1; n <= 5; n++) {
				$('thead tr td:nth-of-type('+n+')').addClass('weekday-'+n);
				$('tbody tr th:nth-of-type('+(n+1)+')').addClass('weekday-'+n);
			}
			
			$('.roosterdeel th.top:contains(ma)').text('maandag');
			$('.roosterdeel th.top:contains(di)').text('dinsdag');
			$('.roosterdeel th.top:contains(wo)').text('woensdag');
			$('.roosterdeel th.top:contains(do)').text('donderdag');
			$('.roosterdeel th.top:contains(vr)').text('vrijdag');

			var today = new Date();
			var weekday = today.getDay();
			
			$("td a").contents().unwrap();
			
			$('.roosterdeel').addClass('schedule-wrapper table').removeClass('roosterdeel'); 
		});
	});
});  