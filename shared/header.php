<!DOCTYPE html>
<html lang="nl">
	<head>
		<meta charset="utf-8">
		<meta http-equiv="X-UA-Compatible" content="IE=edge">
		<meta name="viewport" content="width=device-width, initial-scale=1">
		<title>Scholen aan Zee</title>

		<!-- Bootstrap -->
		<link href="<?= SCRIPT_ROOT ?>shared/css/bootstrap.css" rel="stylesheet">
		<link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.1/css/bootstrap-theme.min.css">
		<link href="<?= SCRIPT_ROOT ?>shared/css/style.css" rel="stylesheet">

		<link href="//maxcdn.bootstrapcdn.com/font-awesome/4.2.0/css/font-awesome.min.css" rel="stylesheet">

		<!-- HTML5 shim and Respond.js for IE8 support of HTML5 elements and media queries -->
		<!-- WARNING: Respond.js doesn't work if you view the page via file:// -->
		<!--[if lt IE 9]>
			<script src="https://oss.maxcdn.com/html5shiv/3.7.2/html5shiv.min.js"></script>
			<script src="https://oss.maxcdn.com/respond/1.4.2/respond.min.js"></script>
			<script src="js/css3-mediaqueries.js"></script>
			
			<style>
				#school-row .school-background {
					display:none;
				}
				
				#IE-warning {
					position:fixed;
					bottom:0px;
					z-index:999;
					margin:0;
					width:100%;
					text-align:center;
				}
			</style>
		<![endif]-->
		
		<noscript>
			<style>
				#javascript-warning {
					position:fixed;
					bottom:0px;
					z-index:999;
					margin:0;
					width:100%;
					text-align:center;
				}
			</style>
		</noscript>
	</head>
	<body class="home" data-root="<?= SCRIPT_ROOT ?>">
		<nav class="navbar navbar-transparent navbar-fixed-top big-logo" role="navigation">
			<div class="container">
				<!-- Brand and toggle get grouped for better mobile display -->
				<div class="navbar-header">
					<button type="button" class="navbar-toggle collapsed" data-toggle="collapse" data-target="#bs-example-navbar-collapse-1">
						<span class="sr-only">Toggle navigation</span>
						<span class="icon-bar"></span>
						<span class="icon-bar"></span>
						<span class="icon-bar"></span>
					</button>
					<a class="navbar-brand" href="<?= SCRIPT_ROOT ?>"><img src="<?= SCRIPT_ROOT ?>shared/img/SAZ-logo-header-big.png"></a>
				</div>
		
				<!-- Collect the nav links, forms, and other content for toggling -->
				<div class="collapse navbar-collapse" id="bs-example-navbar-collapse-1">
					<?php 
					$directoryURI = $_SERVER['REQUEST_URI'];
					$path = parse_url($directoryURI, PHP_URL_PATH);
					$components = explode('/', $path);
					@$first_part = $components[2];
					function active($folder) { global $first_part; if ($first_part == $folder) { echo "active"; } }
					?>
					<ul class="nav navbar-nav">
						
					</ul>
					<ul class="nav navbar-nav navbar-right">
						<li class="<?php active(''); ?>"><a href="<?= SCRIPT_ROOT ?>">Home</a></li>
						<li class="<?php echo $active['nieuws']?>"><a href="#">Nieuws</a></li>
						<li class="dropdown">
							<a href="#" class="dropdown-toggle" data-toggle="dropdown" role="button" aria-expanded="false">Locaties <span class="caret"></span></a>
							<ul class="dropdown-menu" role="menu">
								<li class="<?php active('lyceum'); ?>"><a href="<?= SCRIPT_ROOT ?>lyceum/">Lyceum</a></li>
								<li class="<?php active('mavo'); ?>"><a href="<?= SCRIPT_ROOT ?>mavo/">Mavo</a></li>
								<li class="<?php active('beroepsonderwijs'); ?>"><a href="<?= SCRIPT_ROOT ?>beroepsonderwijs/">Beroepsonderwijs</a></li>
								<li class="<?php active('juniorcollege'); ?>"><a href="<?= SCRIPT_ROOT ?>juniorcollege/">Junior College</a></li>
							</ul>
						</li>
						<li class="dropdown">
							<a href="#" class="dropdown-toggle" data-toggle="dropdown" role="button" aria-expanded="false">Organisatie <span class="caret"></span></a>
							<ul class="dropdown-menu" role="menu">
								<li><a href="<?= SCRIPT_ROOT ?>organisatie/over.php">Over Scholen aan Zee</a></li>
								<li class="divider"></li>
								<lh>Onderwijs</lh>
								<li><a href="#">Hoogbegaafdenonderwijs</a></li>
								<li><a href="#">Sportklassen / Sportbegeleiding</a></li>
								<li><a href="#">Cultuurklassen</a></li>
								<li class="divider"></li>
								<lh>Publicaties</lh>
								<li><a href="#">Schoolgids</a></li>
								<li><a href="#">Magazine "Aan Zee"</a></li>
								<li class="divider"></li>
								<lh>Activiteiten</lh>
								<li><a href="#">Academie aan Zee</a></li>
								<li><a href="#">Scholieren op Zee</a></li>
							</ul>
						</li>
						<li class="dropdown">
							<a href="#" class="dropdown-toggle" data-toggle="dropdown" role="button" aria-expanded="false">Informatie <span class="caret"></span></a>
							<ul class="dropdown-menu" role="menu">
								<lh>Algemeen</lh>
								<li><a href="<?= SCRIPT_ROOT ?>agenda.php">Agenda</a></li>
								<li><a href="#">Schoolgids</a></li>
								<li><a href="#">GMR</a></li>
								<li><a href="#">Vacatures</a></li>
								<li><a href="#">ICT-zaken</a></li>
								<li class="divider"></li>
								<lh>Basisschool</lh>
								<li><a href="#">Keuzegids</a></li>
								<li><a href="#">Inschrijven</a></li>
								<li><a href="#">Toelating</a></li>
								<li><a href="#">Open Dagen / Activiteiten</a></li>
								<li><a href="<?= SCRIPT_ROOT ?>informatie/info-voor-leerkrachten.php">Info voor leerkrachten</a></li>
								<li class="divider"></li>
								<lh>Leerlingen</lh>
								<li><a href="#">Scholieren op Zee</a></li>
								<li><a href="#">Maatschappelijke Stage</a></li>
							</ul>
						</li>
					</ul>
				</div><!-- /.navbar-collapse -->
			</div><!-- /.container-fluid -->
		</nav>
		
		<!--[if lt IE 9]>
			<div id="IE-warning" class="alert alert-danger" role="alert">
				<button type="button" class="close" data-dismiss="alert" aria-label="Close"><span aria-hidden="true">&times;</span></button>
				U gebruikt momenteel een <strong>sterk verouderde browser</strong>. Sommige delen van de site kunnen hierdoor mogelijk niet correct werken. We adviseren u te updaten naar een nieuwere browser.
			</div>
		<![endif]-->
		
		<noscript>
			<div id="javascript-warning" class="alert alert-danger" role="alert">
				<button type="button" class="close" data-dismiss="alert" aria-label="Close"><span aria-hidden="true">&times;</span></button>
				Het lijkt erop dat uw browser <strong>geen JavaScript ondersteunt</strong>. Sommige delen van de site kunnen hierdoor mogelijk niet correct werken. We adviseren u JavaScript in te schakelen of te updaten naar een browser met JavaScript-functionaliteit.
			</div>
		</noscript>