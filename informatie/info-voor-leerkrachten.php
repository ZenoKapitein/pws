<?php require_once('../config.php'); include_once('../shared/header.php'); ?>
		<header class="container-fluid parallax" id="header-image">
			<div class="row image small" style="background-image:url('../shared/img/leerkrachten.jpg'); filter:progid:DXImageTransform.Microsoft.AlphaImageLoader(src='../shared/img/leerkrachten.jpg', sizingMethod='scale'); -ms-filter:"progid:DXImageTransform.Microsoft.AlphaImageLoader(src='../shared/img/leerkrachten.jpg',sizingMethod='scale')";">
				<div class="caption col-xs-12">
					<h4>Informatie</h4>
					<h1>Info voor leerkrachten</h1>
				</div>
			</div>
		</header>
		
		<div class="container" id="main-panel">
			<div class="row">
				<div class="col-xs-12 col-md-9 ">
					<h3>Aanmelding Leerlingen voor 2014 - 2015</h3>
					<p><a href="#"><i class="fa fa-file-pdf-o"></i> inschrijfformulier 2014-2015</a></p>
					<p>Het inlichtingenformulieren/onderwijskundige rapportage ontvangt u van de OBD of bureau SOS. </p>
					
					<h3>Lezing Academie aan Zee: "Talentontwikkeling: Haal het beste uit jongeren"</h3>
					<p>Op woensdag 4 februari komt Yvonne van Sark, partner YoungWorks en medeauteur van boeken als 'Puberbrein binnenstebuiten' en 'Over de top', op uitnodiging van Academie aan Zee een lezing geven. Medewerkers van Scholen aan Zee, collega's van het primair onderwijs en ouders/verzorgers zijn van harte welkom de lezing bij te wonen. Waar ligt de passie van jongeren en hoe kunnen jongeren tot topprestaties komen? En in welk vakgebied en welke sport of hobby dan ook? Spreker Yvonne van Sark behandelt diverse vragen tijdens de lezing. Onderwerpen als ‘hoe kunnen jongeren excellentie bereiken’ en ‘welke rol speelt de omgeving, zoals ouders en onderwijs, bij de ontplooiing van talent bij jongeren’ komen aan bod.</p>
					<p>Deze boeiende lezing over “Talentontwikkeling: haal het beste uit jongeren” is op woensdag 4 februari in de Agora van  Lyceum aan Zee, Drs. F. Bijlweg 6  in Den Helder. De inloop is vanaf 15.30 uur. Belangstellenden zijn welkom bij deze lezing van Academie aan Zee. De lezing start om 16.00 uur en duurt tot 17.00 uur.</p>

					<p>Aanmelden voor deze lezing kan via <a href="#"><i class="fa fa-send-o"></i> academie@scholenaanzee.nl</a></p>
					
					<h3>Planning Activiteiten 2014 - 2015</h3>
					<p><a href="#"><i class="fa fa-file-pdf-o"></i> Klik hier</a> voor een overzicht van de activiteiten in 2014-2015.</p>
					
					<h3>Vragen / Opmerkingen</h3>
					<p>Heeft u vragen en/of opmerkingen, dan kunt u contact opnemen met de accountmanager basisonderwijs,</p>

					<p>Yvonne Kapiteijn-Baltus<br>
					locatieleider Junior College<br>
					telefoon: 0223 540 310 / 540 228<br>
					e-mail: info@scholenaanzee.nl</p>
				</div>
				<aside class="col-xs-12 col-md-3">
					<h3>Meer in <em>Informatie</em></h3>
					<ul class="nav nav-pills nav-stacked">
						<lh>Algemeen</lh>
						<li role="presentation"><a href="#">Agenda</a></li>
						<li role="presentation"><a href="#">Schoolgids</a></li>
						<li role="presentation"><a href="#">GMR</a></li>
						<li role="presentation"><a href="#">Vacatures</a></li>
						<li role="presentation"><a href="#">ICT-zaken</a></li>
						<li class="divider"></li>
						<lh>Basisschool</lh>
						<li role="presentation"><a href="#">Keuzegids</a></li>
						<li role="presentation"><a href="#">Inschrijven</a></li>
						<li role="presentation"><a href="#">Toelating</a></li>
						<li role="presentation"><a href="#">Open Dagen / Activiteiten</a></li>
						<li class="active" role="presentation"><a href="<?= SCRIPT_ROOT ?>organisatie/info-voor-leerkrachten.php">Info voor leerkrachten</a></li>
						<li class="divider"></li>
						<lh>Leerlingen</lh>
						<li role="presentation"><a href="#">Scholieren op Zee</a></li>
						<li role="presentation"><a href="#">Maatschappelijke Stage</a></li>
					</ul>
				</aside>
			</div>
		</div>

<?php include_once('../shared/footer.php'); ?>