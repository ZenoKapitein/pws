<?php require_once('../config.php'); include_once('../shared/header.php'); ?>
		<header class="container-fluid parallax" id="header-image">
			<div class="row image small" style="background-image:url('../shared/img/over.jpg'); filter:progid:DXImageTransform.Microsoft.AlphaImageLoader(src='../shared/img/over.jpg', sizingMethod='scale'); -ms-filter:"progid:DXImageTransform.Microsoft.AlphaImageLoader(src='../shared/img/over.jpg',sizingMethod='scale')";">
				<div class="caption col-xs-12">
					<h4>Organisatie</h4>
					<h1>Over Scholen aan Zee</h1>
				</div>
			</div>
		</header>
		
		<div class="container" id="main-panel">
			<div class="row">
				<div class="col-xs-12 col-md-9 ">
					<h3>Geschiedenis</h3>
					<p><img src="../shared/img/img2.jpg" width="250"; class="pull-right img-responsive">Lorem ipsum dolor sit amet, consectetur adipiscing elit. Proin egestas volutpat ante sed pretium. Proin tempus aliquet dui, vitae commodo erat varius ac. Quisque aliquam vestibulum tincidunt. Phasellus molestie ligula et purus facilisis volutpat eget nec enim. Ut augue nunc, blandit et blandit non, porta a eros. Suspendisse suscipit suscipit risus ut congue. Maecenas quis magna quis neque consectetur interdum. Praesent gravida luctus nunc, eget aliquam mauris aliquet a. Etiam fermentum porttitor nulla, nec euismod lacus sodales a. Aenean ac rutrum magna. Vestibulum vel mattis nunc. Aenean fermentum consectetur pulvinar.</p>
					
					<p>Aliquam pulvinar iaculis convallis. Fusce auctor elit fermentum tortor convallis sodales. Morbi adipiscing tristique hendrerit. Nulla luctus imperdiet molestie. Sed tempus neque sed ipsum porta venenatis. Praesent non magna elit. Maecenas malesuada velit id neque dignissim molestie.</p>
					
					<p>Ut quis sapien neque. Pellentesque porta sollicitudin sem, et auctor sem vestibulum sed. Donec venenatis posuere faucibus. Cras sed est lacus. Aliquam fermentum sapien vel turpis vestibulum vitae ultrices orci commodo. Suspendisse quis est vel libero vehicula gravida sed eu metus. Aliquam semper euismod leo, a mollis nunc mattis non. Pellentesque hendrerit orci non orci lacinia pretium tempus ac ligula. Aenean posuere lorem facilisis odio lobortis facilisis.</p>
					
					<h3>Missie</h3>
					<p>Ut gravida fringilla viverra. Cras at vehicula lacus. Suspendisse pulvinar nunc ac nunc dictum sodales. Mauris euismod fringilla accumsan. Nulla tincidunt fermentum eros, nec placerat neque sagittis mollis. Aliquam in neque nec dui congue mollis blandit eget odio. Nulla facilisi. Suspendisse suscipit eleifend libero sit amet molestie. Lorem ipsum dolor sit amet, consectetur adipiscing elit. Curabitur adipiscing dapibus nisi, et convallis lectus ultricies a. Quisque bibendum feugiat volutpat.</p>
					
					<p>Donec id magna velit. Aliquam consequat nunc a turpis consectetur egestas. Curabitur eu mauris a enim lobortis rhoncus. Nunc magna nisi, laoreet et scelerisque eget, cursus vitae tellus. Sed eleifend felis id dolor malesuada ac cursus dui facilisis. Nulla facilisi. Duis id commodo diam. Curabitur luctus sapien ac odio tempor pellentesque.</p>
				</div>
				<aside class="col-xs-12 col-md-3">
					<h3>Meer in <em>Organisatie</em></h3>
					<ul class="nav nav-pills nav-stacked">
						<li class="active" role="presentation"><a href="<?= SCRIPT_ROOT ?>organisatie/over.php">Over Scholen aan Zee</a></li>
						<li class="divider"></li>
						<lh>Onderwijs</lh>
						<li role="presentation"><a href="#">Hoogbegaafdenonderwijs</a></li>
						<li role="presentation"><a href="#">Sportklassen / Sportbegeleiding</a></li>
						<li role="presentation"><a href="#">Cultuurklassen</a></li>
						<li class="divider"></li>
						<lh>Publicaties</lh>
						<li role="presentation"><a href="#">Schoolgids</a></li>
						<li role="presentation"><a href="#">Magazine "Aan Zee"</a></li>
						<li class="divider"></li>
						<lh>Activiteiten</lh>
						<li role="presentation"><a href="#">Academie aan Zee</a></li>
						<li role="presentation"><a href="#">Scholieren op Zee</a></li>
					</ul>
				</aside>
			</div>
		</div>

<?php include_once('../shared/footer.php'); ?>