<?php require_once('../config.php'); include_once('../shared/header.php');  ?>
		<header class="container-fluid parallax" id="header-image">
			<div class="row image small" style="background-image:url('../shared/img/juco-medium.jpg'); filter:progid:DXImageTransform.Microsoft.AlphaImageLoader(src='../shared/img/juco-medium.jpg', sizingMethod='scale'); -ms-filter:"progid:DXImageTransform.Microsoft.AlphaImageLoader(src='../shared/img/juco-medium.jpg',sizingMethod='scale')";">
				<div class="caption col-xs-12">
					<h4>Schoolpagina:</h4>
					<h1>Junior College</h1>
				</div>
			</div>
		</header>
		
		<div class="container juniorcollege" id="main-panel">
			<div class="row">
				<ul class="nav nav-blocks nav-justified" id="primary-links">
					<li role="presentation"><a href="#"><h3>Roosters</h3></a></li>
					<li role="presentation" class="dropdown">
						<a class="dropdown-toggle" data-toggle="dropdown" href="#" role="button" aria-expanded="false"><h3>ELO</h3></a>
						<ul class="dropdown-menu" role="menu">
							<li role="presentation"><a href="#">Lyceum</a></li>
							<li role="presentation"><a href="#">Mavo</a></li>
							<li role="presentation"><a href="#">Beroepsonderwijs</a></li>
						</ul>
					</li>
					<li role="presentation" class="dropdown">
						<a class="dropdown-toggle" data-toggle="dropdown" href="#" role="button" aria-expanded="false"><h3>Magister</h3></a>
						<ul class="dropdown-menu" role="menu">
							<li role="presentation"><a href="#">Lyceum</a></li>
							<li role="presentation"><a href="#">Mavo</a></li>
							<li role="presentation"><a href="#">Beroepsonderwijs</a></li>
						</ul>
					</li>
					<li role="presentation"><a href="#"><h3>Fotoboeken</h3></a></li>
				</ul>
				<ul class="nav nav-blocks nav-justified" id="secondary-links">
					<li role="presentation"><a href="../agenda.php">Agenda</a></li>
					<li role="presentation"><a href="#">Sportklas</a></li>
					<li role="presentation"><a href="#">MAS</a></li>
					<li role="presentation"><a href="#">Nieuwsbrieven</a></li>
					<li role="presentation" class="dropdown">
						<a class="dropdown-toggle" data-toggle="dropdown" href="#" role="button" aria-expanded="false">
							Ouders
						</a>
						<ul class="dropdown-menu" role="menu">
							<li role="presentation"><a href="#">Magister <i class="fa fa-external-link"></i></a></li>
							<li role="presentation"><a href="#">Absentie</a></li>
							<li role="presentation"><a href="#">Schoolgids <i class="fa fa-cloud-download"></i></a></li>
						</ul>
					</li>
					<li role="presentation" class="dropdown">
						<a class="dropdown-toggle" data-toggle="dropdown" href="#" role="button" aria-expanded="false">
							Personeel
						</a>
						<ul class="dropdown-menu" role="menu">
							<li role="presentation"><a href="#">YouForce</a></li>
							<li role="presentation"><a href="#">Quickcard Werving <i class="fa fa-cloud-download"></i></a></li>
							<li role="presentation"><a href="#">Quickcard Verzuim <i class="fa fa-cloud-download"></i></a></li>
						</ul>
					</li>
					<li role="presentation"><a href="#">Contact</a></li>
				</ul>
			</div>
			
			<div class="row">
				<div class="col-xs-12 col-md-8" id="announcements">
					<header class="bordered clearfix">
						<h2 clas="pull-left">Mededelingen</h2> 
						<div class="btn-toolbar pull-right" id="announcement-filter" role="toolbar">
							<div class="btn-group" data-toggle="tooltip" data-placement="top" title="Klas" role="group">
								<button type="button" class="btn btn-default" data-filter="1">1</button>
								<button type="button" class="btn btn-default" data-filter="2">2</button>
								<button type="button" class="btn btn-default" data-filter="3">3</button>
								<button type="button" class="btn btn-default" data-filter="4">4</button>
							</div>
						</div>
					</header>
					
					<ul class="list-unstyled">
						<li class="announcement" data-groups="">
							<h4>Uitslag Junior Run</h4>
							<p><a href="http://www.scholenaanzee.nl/Media/download/140986/Uitslag%20Junior%20Run%202014.xlsx">Klik op deze link om de uitslag te bekijken</a></p>
						</li>
					</ul>
					
					<header class="bordered clearfix">
						<h2>Nieuws</h2>
					</header>
					<ul class="news list-unstyled list-divided">
						<li class="clearfix">
							<img src="http://lorempixel.com/75/75/?i=1" class="news-image pull-left" />
							<div class="news-content clearfix">
								<h4>Junior College haalt €3.700 op met kerstactie</h4>
								<p>Alle leerlingen hebben zich ingespannen door middel van een uitgebreide sponsorloop - dansen, hardlopen, mountainbiken, rolstoeldansen - en een grote gevarieerde kerstmarkt in het atrium van de school. Veel belangstellenden hebben de activiteiten bezocht en het is nog nooit zo druk geweest. Veel ondernemers uit Julianadorp hadden artikelen beschikbaar gesteld voor de kerstmarkt. Alles bij elkaar heeft dit een prachtig bedrag opgeleverd voor het goede doel, de organisatie ´Het Vergeten Kind´. <a href="../nieuws/junior-college-haalt-3700-op-met-kerstactie.php">Lees meer &raquo;</a></p>
							</div>
							<ul class="news-meta list-inline">
								<li><i class="fa fa-calendar-o"></i><span class="sr-only">Geplaatst op:</span> 19-12-2014</li>
								<li><i class="fa fa-clock-o"></i><span class="sr-only">Tijd:</span> 14:06</li>
								<li><i class="fa fa-map-marker"></i><a href="juniorcollege/"><span class="sr-only">Vestiging(en):</span> Junior College</a></li>
							</ul>
						</li>
						<li class="clearfix">
							<img src="http://lorempixel.com/75/75/?i=2" class="news-image pull-left" />
							<div class="news-content">
								<h4>Mavoleerlingen doen mee aan actie "Buiten de Boot"</h4>
								<p>Evenals vorig jaar zijn mavoleerlingen in het kader van hun maatschappelijke stage druk bezig geweest met de actie 'Buiten de Boot'. Het initiatief van deze actie komt van Stichting Present. De leerlingen hebben geld ingezameld door o.a. de verkoop van zelfgemaakte etenswaren, snacks en het organiseren van een loterij. <a href="../nieuws/mavoleerlingen-doen-mee-aan-actie-buiten-de-boot.php">Lees meer &raquo;</a></p>
							</div>
							<ul class="news-meta list-inline">
								<li><i class="fa fa-calendar-o"></i><span class="sr-only">Geplaatst op:</span> 17-12-2014</li>
								<li><i class="fa fa-clock-o"></i><span class="sr-only">Tijd:</span> 12:35</li>
								<li><i class="fa fa-map-marker"></i><a href="mavo/"><span class="sr-only">Vestiging(en):</span> Mavo aan Zee</a></li>
							</ul>
						</li>
						<li class="clearfix">
							<img src="http://lorempixel.com/75/75/?i=3" class="news-image pull-left" />
							<div class="news-content">
								<h4>Leerlingen Beroepsonderwijs treden op in Den Koogh</h4>
								<p>Dinsdag 9 december hebben leerlingen van muziekmodule The Voice in verzorgingstehuis Den Koogh op enthousiaste wijze acte de présence gegeven. De bewoners genoten zichtbaar. Met het optreden werd een vervolg gegeven aan een inmiddels rijke traditie van zangoptredens in Den Koogh door leerlingen van Beroepsonderwijs aan Zee. <a href="../nieuws/leerlingen-beroepsonderwijs-treden-op-in-den-koogh.php">Lees meer &raquo;</a></p>
							</div>
							<ul class="news-meta list-inline">
								<li><i class="fa fa-calendar-o"></i><span class="sr-only">Geplaatst op:</span> 12-12-2014</li>
								<li><i class="fa fa-clock-o"></i><span class="sr-only">Tijd:</span> 21:11</li>
								<li><i class="fa fa-map-marker"></i><a href="beroepsonderwijs/"><span class="sr-only">Vestiging(en):</span> Beroepsonderwijs aan Zee</a></li>
							</ul>
						</li>
						<li class="clearfix">
							<img src="http://lorempixel.com/75/75/?i=4" class="news-image pull-left" />
							<div class="news-content">
								<h4>Polder Résidence op bezoek bij beroepsonderwijs Junior College</h4>
								<p>Woon-/zorgcomplex Polder Résidence (Breezand) heeft op donderdag 27 november een bezoek gebracht aan het Junior College. Klas JD1J heeft de mensen samen met de docenten Mens & Dienstverlenen Manta Pors en Thelma Grippeling ontvangen. Het was een geweldige, leerzame en ook een heel plezierige dag! <a href="#">Lees meer &raquo;</a></p>
							</div>
							<ul class="news-meta list-inline">
								<li><i class="fa fa-calendar-o"></i><span class="sr-only">Geplaatst op:</span> 02-12-2014</li>
								<li><i class="fa fa-clock-o"></i><span class="sr-only">Tijd:</span> 11:11</li>
								<li><i class="fa fa-map-marker"></i><a href="juniorcollege/"><span class="sr-only">Vestiging(en):</span> Junior College</a></li>
							</ul>
						</li>
						<li class="clearfix">
							<img src="http://lorempixel.com/75/75/?i=5" class="news-image pull-left" />
							<div class="news-content">
								<h4>Prijsuitreiking Seven Days of Feedback in Heineken Music Hall</h4>
								<p>Leerlingen van Mavo aan Zee zijn gisteren op de tweede plaats geëindigd bij de grootste lifestyle challenge van het jaar: Seven Days of Feedback! Een hele prestatie. De vijf beste scholen waren uitgenodigd voor een neonparty in de Heineken Music Hall, waar door Rens Kroes en Tabe Ydo de winnaar bekend werd gemaakt. Het werd een superfeest en de leerlingen hebben genoten. <a href="#">Lees meer &raquo;</a></p>
							</div>
							<ul class="news-meta list-inline">
								<li><i class="fa fa-calendar-o"></i><span class="sr-only">Geplaatst op:</span> 26-11-2014</li>
								<li><i class="fa fa-clock-o"></i><span class="sr-only">Tijd:</span> 08:26</li>
								<li><i class="fa fa-map-marker"></i><a href="mavo/"><span class="sr-only">Vestiging(en):</span> Mavo aan Zee</a></li>
							</ul>
						</li>
						<li class="clearfix">
							<img src="http://lorempixel.com/75/75/?i=6" class="news-image pull-left" />
							<div class="news-content">
								<h4>Workshops Techniek en Vakmanschap groot succes voor leerlingen groep 8.</h4>
								<p>Woensdag 19 november zijn op het Junior College workshops Techniek en Vakmanschap van start gegaan voor leerlingen - die dat leuk vinden - van groep 8 uit de regio. Een grote groep is enthousiast begonnen met het maken van werkstukjes. <a href="#">Lees meer &raquo;</a></p>
							</div>
							<ul class="news-meta list-inline">
								<li><i class="fa fa-calendar-o"></i><span class="sr-only">Geplaatst op:</span> 20-11-2014</li>
								<li><i class="fa fa-clock-o"></i><span class="sr-only">Tijd:</span> 10:54</li>
								<li><i class="fa fa-map-marker"></i><span class="sr-only">Vestiging(en):</span> Alle Vestigingen</li>
							</ul>
						</li>
					</ul>
				</div>
				<aside class="col-xs-12 col-md-4 sidebar">
					<div class="col-xs-12 col-sm-6 col-md-12">
						<h2>Agenda</h2>
						<ul class="mini-calendar list-unstyled list-divided">
							<li class="calendar-item clearfix">
								<div class="event-date pull-left">
									<div class="date-number">15</div>
									<div class="date-month">JAN</div>
								</div>
								<div class="read-more-link pull-right"> <a href="../agenda.php"><i class="fa fa-chevron-right"></i></a> </div>
								<div class="event-description">
									<h5>Titel van gebeurtenis</h5>
									<p class="event-time"><span class="sr-only">Tijd: </span>13:00 - 16:00</p>
									<p class="event-location"><span class="sr-only">Locatie: </span>Lyceum aan Zee</p>
								</div>
							</li>
							<li class="calendar-item clearfix">
								<div class="event-date pull-left">
									<div class="date-number">23</div>
									<div class="date-month">JAN</div>
								</div>
								<div class="read-more-link pull-right"> <a href="../agenda.php"><i class="fa fa-chevron-right"></i></a> </div>
								<div class="event-description">
									<h5>Open Dag</h5>
									<p class="event-time"><span class="sr-only">Tijd: </span>16:00 - 20:00</p>
									<p class="event-location"><span class="sr-only">Locatie: </span>Lyceum aan Zee</p>
								</div>
							</li>
							<li class="calendar-item clearfix">
								<div class="event-date pull-left">
									<div class="date-number">19</div>
									<div class="date-month">FEB</div>
								</div>
								<div class="read-more-link pull-right"> <a href="../agenda.php"><i class="fa fa-chevron-right"></i></a> </div>
								<div class="event-description">
									<h5>Skippyballwedstrijd</h5>
									<p class="event-time"><span class="sr-only">Tijd: </span>13:00 - 16:00</p>
									<p class="event-location"><span class="sr-only">Locatie: </span>Sport aan Zee 2</p>
								</div>
							</li>
						</ul>
					</div>
					<div class="col-xs-12 col-sm-6 col-md-12">
						<h2>Schoolkrant</h2>
						<a class="twitter-timeline" data-chrome="nofooter noheader noborders transparent" data-tweet-limit="3" href="https://twitter.com/OldSchool_NL" data-widget-id="551558703854661633">Tweets door @OldSchool_NL</a> <script>!function(d,s,id){var js,fjs=d.getElementsByTagName(s)[0],p=/^http:/.test(d.location)?'http':'https';if(!d.getElementById(id)){js=d.createElement(s);js.id=id;js.src=p+"://platform.twitter.com/widgets.js";fjs.parentNode.insertBefore(js,fjs);}}(document,"script","twitter-wjs");</script>
					</div>
				</aside>
			</div>
		</div>
		
<?php include_once('../shared/footer.php'); ?>