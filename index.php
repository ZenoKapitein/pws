<?php require_once('config.php'); include_once('shared/header.php'); ?>
		<header class="container-fluid" id="header-image">
			<div class="row">
				<div class="col-xs-12" id="carousel-wrapper">
					<div id="header-carousel" class="carousel slide" data-ride="carousel">
						<!-- Indicators -->
						<ol class="carousel-indicators">
							<li data-target="#header-carousel" data-slide-to="0" class="active"></li>
							<li data-target="#header-carousel" data-slide-to="1"></li>
							<li data-target="#header-carousel" data-slide-to="2"></li>
							<li data-target="#header-carousel" data-slide-to="3"></li>
						</ol>
					
						<!-- Wrapper for slides -->
						<div class="carousel-inner" role="listbox">
							<div class="item active" style="background-image:url(shared/img/img4.jpg); background-position: center top; filter:progid:DXImageTransform.Microsoft.AlphaImageLoader(src='shared/img/img4.jpg', sizingMethod='scale'); -ms-filter:"progid:DXImageTransform.Microsoft.AlphaImageLoader(src='shared/img/img4.jpg',sizingMethod='scale')";">
								<div class="carousel-caption">
									<h1>Nieuwjaarsduik een groot succes</h1>
									<p>Meer dan 1000 scholieren sprongen het ijskoude water in!</p>
									<a href="nieuws/nieuwjaarsduik-een-groot-succes.php" class="btn btn-primary hidden-xs">Lees meer &raquo;</a>
									<a href="nieuws/nieuwjaarsduik-een-groot-succes.php" class="btn btn-primary btn-sm visible-xs-inline-block">Lees meer &raquo;</a>
								</div>
							</div>
							<div class="item active" style="background-image:url(shared/img/img1.jpg); filter:progid:DXImageTransform.Microsoft.AlphaImageLoader(src='shared/img/img1.jpg', sizingMethod='scale'); -ms-filter:"progid:DXImageTransform.Microsoft.AlphaImageLoader(src='shared/img/img1.jpg',sizingMethod='scale')";">
								<div class="carousel-caption">
									<h1>Junior College haalt €3.700 op met kerstactie</h1>
									<p>De kerstactiviteiten van het Junior College op donderdag 18 december zijn bijzonder goed verlopen.</p>
									<a href="#" class="btn btn-primary hidden-xs">Lees meer &raquo;</a>
									<a href="#" class="btn btn-primary btn-sm visible-xs-inline-block">Lees meer &raquo;</a>
								</div>
							</div>
							<div class="item" style="background-image:url(shared/img/img2.jpg); filter:progid:DXImageTransform.Microsoft.AlphaImageLoader(src='shared/img/img2.jpg', sizingMethod='scale'); -ms-filter:"progid:DXImageTransform.Microsoft.AlphaImageLoader(src='shared/img/img2.jpg',sizingMethod='scale')";">
								<div class="carousel-caption">
									<h1><span class="big">Open Dag</span></h1>
									<h3>Vrijdag 23 januari 2014</h3>
									<h4>van 16:00 - 20:00 uur</h4>
									<a href="#" class="btn btn-primary hidden-xs"><i class="fa fa-calendar-o"></i> Bekijk in Agenda</a>
									<a href="#" class="btn btn-primary btn-sm visible-xs-inline-block"><i class="fa fa-calendar-o"></i> Bekijk in Agenda</a>
								</div>
							</div>
							<div class="item" style="background-image:url(shared/img/img3.jpg); filter:progid:DXImageTransform.Microsoft.AlphaImageLoader(src='shared/img/img3.jpg', sizingMethod='scale'); -ms-filter:"progid:DXImageTransform.Microsoft.AlphaImageLoader(src='shared/img/img3.jpg',sizingMethod='scale')";">
								<div class="carousel-caption">
									<h1>Mooie school</h1>
									<p>Lorem ipsum dolor sit amet, consectetur adipiscing elit. Phasellus metus elit, posuere maximus mauris eget, dignissim vehicula ante. </p>
									<a href="#" class="btn btn-primary hidden-xs">Lees meer &raquo;</a>
									<a href="#" class="btn btn-primary btn-sm visible-xs-inline-block">Lees meer &raquo;</a>
								</div>
							</div>
						</div>
					
						<!-- Controls -->
						<a class="left carousel-control" href="#header-carousel" role="button" data-slide="prev">
							<span class="glyphicon glyphicon-chevron-left" aria-hidden="true"></span>
							<span class="sr-only">Previous</span>
						</a>
						<a class="right carousel-control" href="#header-carousel" role="button" data-slide="next">
							<span class="glyphicon glyphicon-chevron-right" aria-hidden="true"></span>
							<span class="sr-only">Next</span>
						</a>
					</div>
				</div>
			</div>
		</header>
		
		<div class="container" id="main-panel">
			<div class="row" id="school-row">
				<a href="lyceum/" class="col-xs-12 col-sm-6 col-md-3 lyceum">
					<div class="school-background" style="background-image:url(shared/img/lyceum-small.png)"></div>
					<h3>Lyceum</h3>
				</a>
				<a href="mavo/" class="col-xs-12 col-sm-6 col-md-3 mavo">
					<div class="school-background" style="background-image:url(shared/img/mavo-small.png)"></div>
					<h3>Mavo</h3>
				</a>
				<a href="beroepsonderwijs/" class="col-xs-12 col-sm-6 col-md-3 beroepsonderwijs">
					<div class="school-background" style="background-image:url(shared/img/beroepsonderwijs-small.png)"></div>
					<h3>Beroepsonderwijs</h3>
				</a>
				<a href="juniorcollege/" class="col-xs-12 col-sm-6 col-md-3 juniorcollege">
					<div class="school-background" style="background-image:url(shared/img/juco-small.png)"></div>
					<h3>Junior College</h3>
				</a>
			</div>
			
			<div class="row">
				<div class="col-xs-12 col-md-8">
					<h3>Nieuws</h3>
					<ul class="news list-unstyled list-divided">
						<li class="clearfix">
							<img src="http://lorempixel.com/75/75/?i=1" class="news-image pull-left" />
							<div class="news-content clearfix">
								<h4>Junior College haalt €3.700 op met kerstactie</h4>
								<p>Alle leerlingen hebben zich ingespannen door middel van een uitgebreide sponsorloop - dansen, hardlopen, mountainbiken, rolstoeldansen - en een grote gevarieerde kerstmarkt in het atrium van de school. Veel belangstellenden hebben de activiteiten bezocht en het is nog nooit zo druk geweest. Veel ondernemers uit Julianadorp hadden artikelen beschikbaar gesteld voor de kerstmarkt. Alles bij elkaar heeft dit een prachtig bedrag opgeleverd voor het goede doel, de organisatie ´Het Vergeten Kind´. <a href="nieuws/junior-college-haalt-3700-op-met-kerstactie.php">Lees meer &raquo;</a></p>
							</div>
							<ul class="news-meta list-inline">
								<li><i class="fa fa-calendar-o"></i><span class="sr-only">Geplaatst op:</span> 19-12-2014</li>
								<li><i class="fa fa-clock-o"></i><span class="sr-only">Tijd:</span> 14:06</li>
								<li><i class="fa fa-map-marker"></i><a href="juniorcollege/"><span class="sr-only">Vestiging(en):</span> Junior College</a></li>
							</ul>
						</li>
						<li class="clearfix">
							<img src="http://lorempixel.com/75/75/?i=2" class="news-image pull-left" />
							<div class="news-content">
								<h4>Mavoleerlingen doen mee aan actie "Buiten de Boot"</h4>
								<p>Evenals vorig jaar zijn mavoleerlingen in het kader van hun maatschappelijke stage druk bezig geweest met de actie 'Buiten de Boot'. Het initiatief van deze actie komt van Stichting Present. De leerlingen hebben geld ingezameld door o.a. de verkoop van zelfgemaakte etenswaren, snacks en het organiseren van een loterij. <a href="nieuws/mavoleerlingen-doen-mee-aan-actie-buiten-de-boot.php">Lees meer &raquo;</a></p>
							</div>
							<ul class="news-meta list-inline">
								<li><i class="fa fa-calendar-o"></i><span class="sr-only">Geplaatst op:</span> 17-12-2014</li>
								<li><i class="fa fa-clock-o"></i><span class="sr-only">Tijd:</span> 12:35</li>
								<li><i class="fa fa-map-marker"></i><a href="mavo/"><span class="sr-only">Vestiging(en):</span> Mavo aan Zee</a></li>
							</ul>
						</li>
						<li class="clearfix">
							<img src="http://lorempixel.com/75/75/?i=3" class="news-image pull-left" />
							<div class="news-content">
								<h4>Leerlingen Beroepsonderwijs treden op in Den Koogh</h4>
								<p>Dinsdag 9 december hebben leerlingen van muziekmodule The Voice in verzorgingstehuis Den Koogh op enthousiaste wijze acte de présence gegeven. De bewoners genoten zichtbaar. Met het optreden werd een vervolg gegeven aan een inmiddels rijke traditie van zangoptredens in Den Koogh door leerlingen van Beroepsonderwijs aan Zee. <a href="nieuws/leerlingen-beroepsonderwijs-treden-op-in-den-koogh.php">Lees meer &raquo;</a></p>
							</div>
							<ul class="news-meta list-inline">
								<li><i class="fa fa-calendar-o"></i><span class="sr-only">Geplaatst op:</span> 12-12-2014</li>
								<li><i class="fa fa-clock-o"></i><span class="sr-only">Tijd:</span> 21:11</li>
								<li><i class="fa fa-map-marker"></i><a href="beroepsonderwijs/"><span class="sr-only">Vestiging(en):</span> Beroepsonderwijs aan Zee</a></li>
							</ul>
						</li>
						<li class="clearfix">
							<img src="http://lorempixel.com/75/75/?i=4" class="news-image pull-left" />
							<div class="news-content">
								<h4>Polder Résidence op bezoek bij beroepsonderwijs Junior College</h4>
								<p>Woon-/zorgcomplex Polder Résidence (Breezand) heeft op donderdag 27 november een bezoek gebracht aan het Junior College. Klas JD1J heeft de mensen samen met de docenten Mens & Dienstverlenen Manta Pors en Thelma Grippeling ontvangen. Het was een geweldige, leerzame en ook een heel plezierige dag! <a href="#">Lees meer &raquo;</a></p>
							</div>
							<ul class="news-meta list-inline">
								<li><i class="fa fa-calendar-o"></i><span class="sr-only">Geplaatst op:</span> 02-12-2014</li>
								<li><i class="fa fa-clock-o"></i><span class="sr-only">Tijd:</span> 11:11</li>
								<li><i class="fa fa-map-marker"></i><a href="juniorcollege/"><span class="sr-only">Vestiging(en):</span> Junior College</a></li>
							</ul>
						</li>
						<li class="clearfix">
							<img src="http://lorempixel.com/75/75/?i=5" class="news-image pull-left" />
							<div class="news-content">
								<h4>Prijsuitreiking Seven Days of Feedback in Heineken Music Hall</h4>
								<p>Leerlingen van Mavo aan Zee zijn gisteren op de tweede plaats geëindigd bij de grootste lifestyle challenge van het jaar: Seven Days of Feedback! Een hele prestatie. De vijf beste scholen waren uitgenodigd voor een neonparty in de Heineken Music Hall, waar door Rens Kroes en Tabe Ydo de winnaar bekend werd gemaakt. Het werd een superfeest en de leerlingen hebben genoten. <a href="#">Lees meer &raquo;</a></p>
							</div>
							<ul class="news-meta list-inline">
								<li><i class="fa fa-calendar-o"></i><span class="sr-only">Geplaatst op:</span> 26-11-2014</li>
								<li><i class="fa fa-clock-o"></i><span class="sr-only">Tijd:</span> 08:26</li>
								<li><i class="fa fa-map-marker"></i><a href="mavo/"><span class="sr-only">Vestiging(en):</span> Mavo aan Zee</a></li>
							</ul>
						</li>
						<li class="clearfix">
							<img src="http://lorempixel.com/75/75/?i=6" class="news-image pull-left" />
							<div class="news-content">
								<h4>Workshops Techniek en Vakmanschap groot succes voor leerlingen groep 8.</h4>
								<p>Woensdag 19 november zijn op het Junior College workshops Techniek en Vakmanschap van start gegaan voor leerlingen - die dat leuk vinden - van groep 8 uit de regio. Een grote groep is enthousiast begonnen met het maken van werkstukjes. <a href="#">Lees meer &raquo;</a></p>
							</div>
							<ul class="news-meta list-inline">
								<li><i class="fa fa-calendar-o"></i><span class="sr-only">Geplaatst op:</span> 20-11-2014</li>
								<li><i class="fa fa-clock-o"></i><span class="sr-only">Tijd:</span> 10:54</li>
								<li><i class="fa fa-map-marker"></i><span class="sr-only">Vestiging(en):</span> Alle Vestigingen</li>
							</ul>
						</li>
					</ul>
				</div>
				<aside class="col-xs-12 col-md-4 sidebar">
					<div class="col-xs-12 col-sm-6 col-md-12">
						<h3>Agenda</h3>
						<ul class="mini-calendar list-unstyled list-divided">
							<li class="calendar-item clearfix">
								<div class="event-date pull-left">
									<div class="date-number">15</div>
									<div class="date-month">JAN</div>
								</div>
								<div class="read-more-link pull-right"> <a href="../agenda.php"><i class="fa fa-chevron-right"></i></a> </div>
								<div class="event-description">
									<h5>Titel van gebeurtenis</h5>
									<p class="event-time"><span class="sr-only">Tijd: </span>13:00 - 16:00</p>
									<p class="event-location"><span class="sr-only">Locatie: </span>Lyceum aan Zee</p>
								</div>
							</li>
							<li class="calendar-item clearfix">
								<div class="event-date pull-left">
									<div class="date-number">23</div>
									<div class="date-month">JAN</div>
								</div>
								<div class="read-more-link pull-right"> <a href="../agenda.php"><i class="fa fa-chevron-right"></i></a> </div>
								<div class="event-description">
									<h5>Open Dag</h5>
									<p class="event-time"><span class="sr-only">Tijd: </span>16:00 - 20:00</p>
									<p class="event-location"><span class="sr-only">Locatie: </span>Lyceum aan Zee</p>
								</div>
							</li>
							<li class="calendar-item clearfix">
								<div class="event-date pull-left">
									<div class="date-number">19</div>
									<div class="date-month">FEB</div>
								</div>
								<div class="read-more-link pull-right"> <a href="../agenda.php"><i class="fa fa-chevron-right"></i></a> </div>
								<div class="event-description">
									<h5>Skippyballwedstrijd</h5>
									<p class="event-time"><span class="sr-only">Tijd: </span>13:00 - 16:00</p>
									<p class="event-location"><span class="sr-only">Locatie: </span>Sport aan Zee 2</p>
								</div>
							</li>
						</ul>
					</div>
					<div class="col-xs-12 col-sm-6 col-md-12">
						<h3>Social</h3>
						<a class="twitter-timeline" data-chrome="nofooter noheader noborders transparent" data-tweet-limit="5" href="https://twitter.com/zenoachtig" data-widget-id="549719104526249984">Tweets door @zenoachtig</a> <script>!function(d,s,id){var js,fjs=d.getElementsByTagName(s)[0],p=/^http:/.test(d.location)?'http':'https';if(!d.getElementById(id)){js=d.createElement(s);js.id=id;js.src=p+"://platform.twitter.com/widgets.js";fjs.parentNode.insertBefore(js,fjs);}}(document,"script","twitter-wjs");</script>
					</div>
				</aside>
			</div>
		</div>
		
<?php include_once('shared/footer.php')?>