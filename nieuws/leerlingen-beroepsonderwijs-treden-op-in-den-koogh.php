<?php require_once('../config.php'); include_once('../shared/header.php'); ?>
		<header class="container-fluid parallax" id="header-image">
			<div class="row image large" style="background-image:url('http://lorempixel.com/1200/800/?i=5'); filter:progid:DXImageTransform.Microsoft.AlphaImageLoader(src='../shared/img/img4.jpg', sizingMethod='scale'); -ms-filter:"progid:DXImageTransform.Microsoft.AlphaImageLoader(src='../shared/img/img4.jpg',sizingMethod='scale')";">
			</div>
		</header>
		
		<div class="container" id="main-panel">
			<div class="row">
				<div class="col-xs-12 col-md-9 ">
					<h1>Leerlingen Beroepsonderwijs treden op in Den Koogh</h1>
					<ul class="news-meta list-inline" >
						<li><i class="fa fa-calendar-o"></i><span class="sr-only">Geplaatst op:</span> 12-12-2014</li>
						<li><i class="fa fa-clock-o"></i><span class="sr-only">Tijd:</span> 21:11</li>
						<li><i class="fa fa-map-marker"></i><a href="../../beroepsonderwijs/"><span class="sr-only">Vestiging(en):</span> Beroepsonderwijs aan Zee</a></li>
					</ul>
					<hr>
					<p><strong>Dinsdag 9 december hebben leerlingen van muziekmodule The Voice in verzorgingstehuis Den Koogh op enthousiaste wijze acte de présence gegeven. De bewoners genoten zichtbaar. Met het optreden werd een vervolg gegeven aan een inmiddels rijke traditie van zangoptredens in Den Koogh door leerlingen van Beroepsonderwijs aan Zee.</strong></p>
					
					<p>Lorem ipsum dolor sit amet, consectetur adipiscing elit. Proin egestas volutpat ante sed pretium. Proin tempus aliquet dui, vitae commodo erat varius ac. Quisque aliquam vestibulum tincidunt. Phasellus molestie ligula et purus facilisis volutpat eget nec enim. Ut augue nunc, blandit et blandit non, porta a eros. Suspendisse suscipit suscipit risus ut congue. Maecenas quis magna quis neque consectetur interdum. Praesent gravida luctus nunc, eget aliquam mauris aliquet a. Etiam fermentum porttitor nulla, nec euismod lacus sodales a. Aenean ac rutrum magna. Vestibulum vel mattis nunc. Aenean fermentum consectetur pulvinar.</p>
					
					<p><img src="http://lorempixel.com/700/400/?i=5" width="250"; class="pull-left img-responsive">Aliquam pulvinar iaculis convallis. Fusce auctor elit fermentum tortor convallis sodales. Morbi adipiscing tristique hendrerit. Nulla luctus imperdiet molestie. Sed tempus neque sed ipsum porta venenatis. Praesent non magna elit. Maecenas malesuada velit id neque dignissim molestie.</p>
					
					<p>Ut quis sapien neque. Pellentesque porta sollicitudin sem, et auctor sem vestibulum sed. Donec venenatis posuere faucibus. Cras sed est lacus. Aliquam fermentum sapien vel turpis vestibulum vitae ultrices orci commodo. Suspendisse quis est vel libero vehicula gravida sed eu metus. Aliquam semper euismod leo, a mollis nunc mattis non. Pellentesque hendrerit orci non orci lacinia pretium tempus ac ligula. Aenean posuere lorem facilisis odio lobortis facilisis. Proin egestas volutpat ante sed pretium. Maecenas quis magna quis neque consectetur interdum.</p>
					
					<p><img src="http://lorempixel.com/700/400/?i=6" width="250"; class="pull-right img-responsive">Ut gravida fringilla viverra. Cras at vehicula lacus. Suspendisse pulvinar nunc ac nunc dictum sodales. Mauris euismod fringilla accumsan. Nulla tincidunt fermentum eros, nec placerat neque sagittis mollis. Aliquam in neque nec dui congue mollis blandit eget odio. Nulla facilisi. Suspendisse suscipit eleifend libero sit amet molestie. Lorem ipsum dolor sit amet, consectetur adipiscing elit. Curabitur adipiscing dapibus nisi, et convallis lectus ultricies a. Quisque bibendum feugiat volutpat.</p>
					
					<p>Donec id magna velit. Aliquam consequat nunc a turpis consectetur egestas. Curabitur eu mauris a enim lobortis rhoncus. Nunc magna nisi, laoreet et scelerisque eget, cursus vitae tellus. Sed eleifend felis id dolor malesuada ac cursus dui facilisis. Nulla facilisi. Duis id commodo diam. Curabitur luctus sapien ac odio tempor pellentesque.</p>
				</div>
				<aside class="col-xs-12 col-md-3">
					<h3>Meer Nieuws</h3>
					<ul class="news list-unstyled list-divided">
						<li class="clearfix">
							<a href="nieuwjaarsduik-een-groot-succes.php">
								<img src="http://lorempixel.com/750/350/?i=1" style="margin-bottom:10px;" class="img-responsive" />
								<div class="news-content clearfix">
									<h4>Nieuwjaarsduik een groot succes</h4>
								</div>
							</a>
						</li>
						<li class="clearfix">
							<a href="junior-college-haalt-3700-op-met-kerstactie.php">
								<img src="http://lorempixel.com/750/350/?i=2" style="margin-bottom:10px;" class="img-responsive" />
								<div class="news-content clearfix">
									<h4>Junior College haalt €3.700 op met kerstactie</h4>
								</div>
							</a>
						</li>
						<li class="clearfix">
							<a href="mavoleerlingen-doen-mee-aan-actie-buiten-de-boot.php">
								<img src="http://lorempixel.com/750/350/?i=3" style="margin-bottom:10px;" class="img-responsive" />
								<div class="news-content">
									<h4>Mavoleerlingen doen mee aan actie "Buiten de Boot"</h4>
								</div>
							</a>
						</li>
						<li class="clearfix">
							<a href="#">
								<img src="http://lorempixel.com/750/350/?i=4" style="margin-bottom:10px;" class="img-responsive" />
								<div class="news-content">
									<h4>Polder Résidence op bezoek bij beroepsonderwijs Junior College</h4>
								</div>
							</a>
						</li>
					</ul>
				</aside>
			</div>
		</div>

<?php include_once('../shared/footer.php'); ?>