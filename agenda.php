<?php require_once('config.php'); include_once('shared/header.php'); ?>
		<header class="container-fluid parallax" id="header-image">
			<div class="row image small" style="background-image:url('../shared/img/notebloc.jpg'); filter:progid:DXImageTransform.Microsoft.AlphaImageLoader(src='../shared/img/notebloc.jpg', sizingMethod='scale'); -ms-filter:"progid:DXImageTransform.Microsoft.AlphaImageLoader(src='../shared/img/notebloc.jpg',sizingMethod='scale')";">
				<div class="caption col-xs-12">
					<h1>Agenda</h1>
				</div>
			</div>
		</header>
		
		<div class="container" id="main-panel">
			<div class="row">
				<div class="col-xs-12">
					<?php 
						$today = date('U');
						$month = date('m');
						$year = date('Y');
						$start_of_month = "01-$month-$year";
							
						$month = [
							'month_number' => date('m', strtotime($start_of_month)),
							'day_of_week' => date('N', strtotime($start_of_month)),
							'number_of_days' => date('t', strtotime($start_of_month))
						];
					?>
					
					<table style="margin-top:15px;" class="table table-bordered calendar">	
						<thead>
							<th colspan="5">
								<a href="#"><i class="fa fa-chevron-left"></i></a>
								<?php // echo ucfirst(strftime('%B %Y', strtotime($start_of_month))); ?>
								Februari 2015
								<a href="#"><i class="fa fa-chevron-right"></i></a>
							</th>
						</thead>
						<tr>
							<td><div class='date-number'>02</div></td>
							<td>
								<div class='date-number'>03</div>
								<ul class="event-list list-unstyled">
									<li class="event lyceum">
										<a href="#" data-toggle="popover" tabindex="0" trigger="focus" data-container="body" data-toggle="popover" data-placement="top" title="Details">
											<div class="popoverContent hidden">
												<ul class="list-unstyled">
													<li><strong>Gesprekken met mentoren leerjaar 3</strong></li>
													<li><i class="fa fa-fw fa-calendar-o"></i> 3 februari 2015</li>
													<li><i class="fa fa-fw fa-building-o"></i><span class="sr-only">Vestiging(en):</span> Lyceum aan Zee</li>
												</ul>
											</div>
											<span class="event-color"></span>
											Gesprekken met mentoren leerjaar 3
										</a>
									</li>
								</ul>
							</td>
							<td>
								<div class='date-number'>04</div>
								<ul class="event-list list-unstyled">
									<li class="event beroepsonderwijs">
										<a href="#" data-toggle="popover" tabindex="0" trigger="focus" data-container="body" data-toggle="popover" data-placement="top" title="Details">
											<div class="popoverContent hidden">
												<ul class="list-unstyled">
													<li><strong>Volleybaltoernooi klas 3</strong></li>
													<li><i class="fa fa-fw fa-calendar-o"></i> 4 februari 2015</li>
													<li><i class="fa fa-fw fa-clock-o"></i><span class="sr-only">Tijd:</span> 13:00 – 15:00</li>
													<li><i class="fa fa-fw fa-building-o"></i><span class="sr-only">Vestiging(en):</span> Mavo aan Zee</li>
													<li><i class="fa fa-fw fa-map-marker"></i><span class="sr-only">Locatie:</span> Sport aan Zee 2</li>
												</ul>
											</div>
											<span class="event-color"></span>
											Volleybaltoernooi leerjaar 3
										</a>
									</li>
									<li class="event mavo">
										<a href="#" data-toggle="popover" tabindex="0" trigger="focus" data-container="body" data-toggle="popover" data-placement="top" title="Details">
											<div class="popoverContent hidden">
												<ul class="list-unstyled">
													<li><strong>Volleybaltoernooi klas 3</strong></li>
													<li><i class="fa fa-fw fa-calendar-o"></i> 4 februari 2015</li>
													<li><i class="fa fa-fw fa-building-o"></i><span class="sr-only">Vestiging(en):</span> Beroepsonderwijs aan Zee</li>
												</ul>
											</div>
											<span class="event-color"></span>
											Volleybaltoernooi klas 3
										</a>
									</li>
								</ul>
							</td>
							<td><div class='date-number'>05</div></td>
							<td><div class='date-number'>06</div></td>
						</tr>
						<tr>
							<td>
								<div class='date-number'>09</div>
								<ul class="event-list list-unstyled">
									<li class="event lyceum">
										<a href="#" data-toggle="popover" tabindex="0" trigger="focus" data-container="body" data-toggle="popover" data-placement="top" title="Details">
											<div class="popoverContent hidden">
												<ul class="list-unstyled">
													<li><strong>Herkansing Toetsweek 2</strong></li>
													<li><i class="fa fa-fw fa-calendar-o"></i> 9 februari 2015</li>
													<li><i class="fa fa-fw fa-clock-o"></i><span class="sr-only">Tijd:</span> 8:15 – 12:30</li>
													<li><i class="fa fa-fw fa-building-o"></i><span class="sr-only">Vestiging(en):</span> Lyceum aan Zee</li>
												</ul>
											</div>
											<span class="event-color"></span>
											Herkansing Toetsweek 2
										</a>
									</li>
									<li class="event mavo">
										<a href="#" data-toggle="popover" tabindex="0" trigger="focus" data-container="body" data-toggle="popover" data-placement="top" title="Details">
											<div class="popoverContent hidden">
												<ul class="list-unstyled">
													<li><strong>Ouderavond Klas 4 Examenvoorbereiding</strong></li>
													<li><i class="fa fa-fw fa-calendar-o"></i> 9 februari 2015</li>
													<li><i class="fa fa-fw fa-clock-o"></i><span class="sr-only">Tijd:</span> 20:00 – 21:00</li>
													<li><i class="fa fa-fw fa-building-o"></i><span class="sr-only">Vestiging(en):</span> Mavo aan Zee</li>
												</ul>
											</div>
											<span class="event-color"></span>
											Ouderavond Klas 4 Examenvoorbereiding
										</a>
									</li>
								</ul>
							</td>
							<td>
								<div class='date-number'>10</div>
								<ul class="event-list list-unstyled">
									<li class="event lyceum">
										<a href="#" data-toggle="popover" tabindex="0" trigger="focus" data-container="body" data-toggle="popover" data-placement="top" title="Details">
											<div class="popoverContent hidden">
												<ul class="list-unstyled">
													<li><strong>Voorlichting vakken bovenbouw voor ouders en leerlingen klas 3</strong></li>
													<li><i class="fa fa-fw fa-calendar-o"></i> 10 februari 2015</li>
													<li><i class="fa fa-fw fa-clock-o"></i><span class="sr-only">Tijd:</span> 20:00 – 21:00</li>
													<li><i class="fa fa-fw fa-building-o"></i><span class="sr-only">Vestiging(en):</span> Lyceum aan Zee</li>
												</ul>
											</div>
											<span class="event-color"></span>
											Voorlichting vakken bovenbouw voor ouders en leerlingen klas 3
										</a>
									</li>
									<li class="event lyceum">
										<a href="#" data-toggle="popover" tabindex="0" trigger="focus" data-container="body" data-toggle="popover" data-placement="top" title="Details">
											<div class="popoverContent hidden">
												<ul class="list-unstyled">
													<li><strong>Volleybaltoernooi klas 2</strong></li>
													<li><i class="fa fa-fw fa-calendar-o"></i> 10 februari 2015</li>
													<li><i class="fa fa-fw fa-clock-o"></i><span class="sr-only">Tijd:</span> 14:00 – 16:00</li>
													<li><i class="fa fa-fw fa-building-o"></i><span class="sr-only">Vestiging(en):</span> Lyceum aan Zee</li>
													<li><i class="fa fa-fw fa-map-marker"></i><span class="sr-only">Locatie:</span> Sporthal de Slenk</li>
												</ul>
											</div>
											<span class="event-color"></span>
											Volleybaltoernooi klas 2
										</a>
									</li>
									<li class="event juniorcollege">
										<a href="#" data-toggle="popover" tabindex="0" trigger="focus" data-container="body" data-toggle="popover" data-placement="top" title="Details">
											<div class="popoverContent hidden">
												<ul class="list-unstyled">
													<li><strong>Horeca Road Show tweede klassen</strong></li>
													<li><i class="fa fa-fw fa-calendar-o"></i> 10 februari 2015</li>
													<li><i class="fa fa-fw fa-clock-o"></i><span class="sr-only">Tijd:</span> 14:00 – 16:00</li>
													<li><i class="fa fa-fw fa-building-o"></i><span class="sr-only">Vestiging(en):</span> Junior College</li>
												</ul>
											</div>
											<span class="event-color"></span>
											Horeca Road Show klas tweede klassen
										</a>
									</li>
									<li class="event beroepsonderwijs">
										<a href="#" data-toggle="popover" tabindex="0" trigger="focus" data-container="body" data-toggle="popover" data-placement="top" title="Details">
											<div class="popoverContent hidden">
												<ul class="list-unstyled">
													<li><strong>Carnaval schoolfeest klas 1 en 2</strong></li>
													<li><i class="fa fa-fw fa-calendar-o"></i> 10 februari 2015</li>
													<li><i class="fa fa-fw fa-clock-o"></i><span class="sr-only">Tijd:</span> 19:30 – 22:30</li>
													<li><i class="fa fa-fw fa-building-o"></i><span class="sr-only">Vestiging(en):</span> Beroepsonderwijs aan Zee</li>
												</ul>
											</div>
											<span class="event-color"></span>
											Carnaval schoolfeest klas 1 en 2
										</a>
									</li>
									<li class="event mavo">
										<a href="#" data-toggle="popover" tabindex="0" trigger="focus" data-container="body" data-toggle="popover" data-placement="top" title="Details">
											<div class="popoverContent hidden">
												<ul class="list-unstyled">
													<li><strong>Ouderavond klas 2 vakkenpakketkeuze</strong></li>
													<li><i class="fa fa-fw fa-calendar-o"></i> 10 februari 2015</li>
													<li><i class="fa fa-fw fa-clock-o"></i><span class="sr-only">Tijd:</span> 20:00 – 21:00</li>
													<li><i class="fa fa-fw fa-building-o"></i><span class="sr-only">Vestiging(en):</span> Mavo aan Zee</li>
												</ul>
											</div>
											<span class="event-color"></span>
											Ouderavond klas 2 vakkenpakketkeuze
										</a>
									</li>
								</ul>
							</td>
							<td>
								<div class='date-number'>11</div>
								<ul class="event-list list-unstyled">
									<li class="event mavo">
										<a href="#" data-toggle="popover" tabindex="0" trigger="focus" data-container="body" data-toggle="popover" data-placement="top" title="Details">
											<div class="popoverContent hidden">
												<ul class="list-unstyled">
													<li><strong>Volleybaltoernooi klas 2</strong></li>
													<li><i class="fa fa-fw fa-calendar-o"></i> 11 februari 2015</li>
													<li><i class="fa fa-fw fa-clock-o"></i><span class="sr-only">Tijd:</span> 14:00 – 16:00</li>
													<li><i class="fa fa-fw fa-building-o"></i><span class="sr-only">Vestiging(en):</span> Mavo aan Zee</li>
													<li><i class="fa fa-fw fa-map-marker"></i><span class="sr-only">Locatie:</span> Sporthal de Slenk</li>
												</ul>
											</div>
											<span class="event-color"></span>
											Volleybaltoernooi klas 2
										</a>
									</li>	
									<li class="event beroepsonderwijs">
										<a href="#" data-toggle="popover" tabindex="0" trigger="focus" data-container="body" data-toggle="popover" data-placement="top" title="Details">
											<div class="popoverContent hidden">
												<ul class="list-unstyled">
													<li><strong>Carnaval schoolfeest klas 3 en 4</strong></li>
													<li><i class="fa fa-fw fa-calendar-o"></i> 11 februari 2015</li>
													<li><i class="fa fa-fw fa-clock-o"></i><span class="sr-only">Tijd:</span> 20:00 – 23:00</li>
													<li><i class="fa fa-fw fa-building-o"></i><span class="sr-only">Vestiging(en):</span> Beroepsonderwijs aan Zee</li>
												</ul>
											</div>
											<span class="event-color"></span>
											Carnaval schoolfeest klas 3 en 4
										</a>
									</li>	
								</ul>
							</td>
							<td><div class='date-number'>12</div></td>
							<td><div class='date-number'>13</div></td>
						</tr>
						<tr>
							<td><div class='date-number'>16</div></td>
							<td>
								<div class='date-number'>17</div>
								<ul class="event-list list-unstyled">
									<li class="event lyceum">
										<a href="#" data-toggle="popover" tabindex="0" trigger="focus" data-container="body" data-toggle="popover" data-placement="top" title="Details">
											<div class="popoverContent hidden">
												<ul class="list-unstyled">
													<li><strong>Volleybaltoernooi klas 1</strong></li>
													<li><i class="fa fa-fw fa-calendar-o"></i> 17 februari 2015</li>
													<li><i class="fa fa-fw fa-clock-o"></i><span class="sr-only">Tijd:</span> 14:00 – 16:00</li>
													<li><i class="fa fa-fw fa-building-o"></i><span class="sr-only">Vestiging(en):</span> Lyceum aan Zee</li>
													<li><i class="fa fa-fw fa-map-marker"></i><span class="sr-only">Locatie:</span> Sporthal de Slenk</li>
												</ul>
											</div>
											<span class="event-color"></span>
											Volleybaltoernooi klas 1
										</a>
									</li>	
									<li class="event mavo">
										<a href="#" data-toggle="popover" tabindex="0" trigger="focus" data-container="body" data-toggle="popover" data-placement="top" title="Details">
											<div class="popoverContent hidden">
												<ul class="list-unstyled">
													<li><strong>London/Berlijn klas 3</strong></li>
													<li><i class="fa fa-fw fa-calendar-o"></i> 17 t/m 20 februari 2015</li>
													<li><i class="fa fa-fw fa-clock-o"></i><span class="sr-only">Tijd:</span> 14:00 – 16:00</li>
													<li><i class="fa fa-fw fa-building-o"></i><span class="sr-only">Vestiging(en):</span> Mavo aan Zee</li>
												</ul>
											</div>
											<span class="event-color"></span>
											London/Berlijn klas 3
										</a>
									</li>	
								</ul>
							</td>
							<td>
								<div class='date-number'>18</div>
								<ul class="event-list list-unstyled">
									<li class="event mavo">
										<a href="#" data-toggle="popover" tabindex="0" trigger="focus" data-container="body" data-toggle="popover" data-placement="top" title="Details">
											<div class="popoverContent hidden">
												<ul class="list-unstyled">
													<li><strong>London/Berlijn klas 3</strong></li>
													<li><i class="fa fa-fw fa-calendar-o"></i> 17 t/m 20 februari 2015</li>
													<li><i class="fa fa-fw fa-clock-o"></i><span class="sr-only">Tijd:</span> 14:00 – 16:00</li>
													<li><i class="fa fa-fw fa-building-o"></i><span class="sr-only">Vestiging(en):</span> Mavo aan Zee</li>
												</ul>
											</div>
											<span class="event-color"></span>
											London/Berlijn klas 3
										</a>
									</li>	
								</ul>
							</td>
							<td>
								<div class='date-number'>19</div>
								<ul class="event-list list-unstyled">
									<li class="event mavo">
										<a href="#" data-toggle="popover" tabindex="0" trigger="focus" data-container="body" data-toggle="popover" data-placement="top" title="Details">
											<div class="popoverContent hidden">
												<ul class="list-unstyled">
													<li><strong>London/Berlijn klas 3</strong></li>
													<li><i class="fa fa-fw fa-calendar-o"></i> 17 t/m 20 februari 2015</li>
													<li><i class="fa fa-fw fa-clock-o"></i><span class="sr-only">Tijd:</span> 14:00 – 16:00</li>
													<li><i class="fa fa-fw fa-building-o"></i><span class="sr-only">Vestiging(en):</span> Mavo aan Zee</li>
												</ul>
											</div>
											<span class="event-color"></span>
											London/Berlijn klas 3
										</a>
									</li>	
								</ul></td>
							<td>
								<div class='date-number'>20</div>
								<ul class="event-list list-unstyled">
									<li class="event mavo">
										<a href="#" data-toggle="popover" tabindex="0" trigger="focus" data-container="body" data-toggle="popover" data-placement="top" title="Details">
											<div class="popoverContent hidden">
												<ul class="list-unstyled">
													<li><strong>London/Berlijn klas 3</strong></li>
													<li><i class="fa fa-fw fa-calendar-o"></i> 17 t/m 20 februari 2015</li>
													<li><i class="fa fa-fw fa-clock-o"></i><span class="sr-only">Tijd:</span> 14:00 – 16:00</li>
													<li><i class="fa fa-fw fa-building-o"></i><span class="sr-only">Vestiging(en):</span> Mavo aan Zee</li>
												</ul>
											</div>
											<span class="event-color"></span>
											London/Berlijn klas 3
										</a>
									</li>	
								</ul>
							</td>
						</tr>
						<tr>
							<td class="holiday">
								<div class='date-number'>23</div>
								<div class="holiday-label">
									<strong>Vakantie: </strong>Voorjaarsvakantie 2015
								</div>
							</td>
							<td class="holiday">
								<div class='date-number'>24</div>
								<div class="holiday-label">
									<strong>Vakantie: </strong>Voorjaarsvakantie 2015
								</div>
							</td>
							<td class="holiday">
								<div class='date-number'>25</div>
								<div class="holiday-label">
									<strong>Vakantie: </strong>Voorjaarsvakantie 2015
								</div>
							</td>
							<td class="holiday">
								<div class='date-number'>26</div>
								<div class="holiday-label">
									<strong>Vakantie: </strong>Voorjaarsvakantie 2015
								</div>
							</td>
							<td class="holiday">
								<div class='date-number'>27</div>
								<div class="holiday-label">
									<strong>Vakantie: </strong>Voorjaarsvakantie 2015
								</div>
							</td>
						</tr>
						<?php 
//							$day_of_week = $month['day_of_week'];
//							
//							for($i = 1; $i < $day_of_week; $i++) {
//								echo "<td></td>";
//							}
//							
//							for($i = 01; $i <= $month['number_of_days']; $i++) {
//							
//								if($day_of_week == 8) {
//									echo "</tr>";
//									echo "<tr>";
//									$day_of_week = 1;
//								}
//								
//								echo "<td><div class='date-number'>".date('d', strtotime("$i-".$month['month_number']."-$year"))."</div></td>";
//								
//								$day_of_week++;
//							}
//						
//							for($i = $day_of_week; $i <= 7; $i++) {
//								echo "<td></td>";
//							}
						
						?>
						</tr>
					</table>	
				</div>
			</div>
		</div>
		
<?php include_once('shared/footer.php')?>